package com.quijotelui.clientews;

/**
 *
 * @author jorgequiguango
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        envia();
    }
    
    static void envia() {
        Enviar enviar = new Enviar("/app/Quijotelui/comprobante/firmado/"
                + "1210201701107245887700110010030000012481234567810.xml",
                "/app/Quijotelui/comprobante/enviado",
                "/app/Quijotelui/comprobante/rechazado",
                "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl");
        enviar.executeEnviar();              
        
        Comprobar comprovar = new Comprobar("/app/Quijotelui/autorizado", 
                "/app/Quijotelui/noautorizado", 
                "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl");
        
        comprovar.executeComprobar("051220170110044566770011001002000001079123456781");
    }        
}
