# QuijoteLuiClient

Aplicación para enviar los comprobantes electrónicos a los servicios web del SRI Ecuador

### Comando en terminal para compilar y generar el archivo QuijoteLuiClient-1.2.jar
```
ant

```
### Comando en terminal para subir al repositorio Maven Local (Linux\Mac)
```
mvn install:install-file -Dfile=./dist/QuijoteLuiClient-1.2.jar -DgroupId=com.quijotelui.clientews -DartifactId=QuijoteLuiClient -Dversion=1.2 -Dpackaging=jar
```
### Comando en terminal para subir al repositorio Maven Local (Windows)
```
cd .\dist\
```
```
mvn install:install-file "-Dfile=QuijoteLuiClient-1.2.jar" "-DgroupId=com.quijotelui.clientews" "-DartifactId=QuijoteLuiClient" "-Dversion=1.2" "-Dpackaging=jar"
```
#### Documentación
https://www.allku.expert/quijotelui-client/
